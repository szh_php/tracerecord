# dayu-tracerecord
Access to Dayu workbench interface

## 安装

```bash
composer require dayu/tracerecord
```

## 中间件新增
```bash
protected $middleware = [
  TraceRecord::class
];
```

## config/app.php 
```bash
'providers' => [
    Dayu\Tracerecord\Providers\TraceRecordProvider::class
```


## AppServiceProvider
```bash
$this->app->register(TraceRecordProvider::class);
```

## 发布配置
```bash
php artisan vendor:publish --provider "Dayu\Tracerecord\Providers\TraceRecordProvider"
```

## .env配置

```bash
#开关
TRACERECORD_LOG = true

#最大记录返回值大小 0表示不限制
TRACERECORD_MAX_RESPONSE_SIZE=5

#日志文件路径 默认 'logs/api_log/tracerecord.log' 
TRACERECORD_DEFAULT_PATH=

TRACERECORD_LOG_CHANNEL=log


```