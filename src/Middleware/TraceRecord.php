<?php
namespace Dayu\Tracerecord\Middleware;

use Closure;
use Illuminate\Http\Request;
use Dayu\Tracerecord\Jobs\TraceRecordJobs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TraceRecord
{
    public function handle(Request $request, Closure $next)
    {
        if($this->isOpenRecord($request->method())){
            return $next($request);
        }
        $traceId = md5(time() . mt_rand(1, 1000000));
        // 记录请求信息
        $requestMessage = [
            'traceId' => $traceId,
            'url' => $request->url(),
            'method' => $request->method(),
            'ip' => $request->ips(),
            'user_id' =>Auth::guard('api')->id(),
            'params' => count($request->allFiles())?'files':$request->all()
        ];
        $response = $next($request);
        $responseData = [
            'traceId' => $traceId,
            'response' => json_decode($response->getContent(), true) ?? ""
        ];
        dispatch(new TraceRecordJobs($requestMessage,$responseData));
        return $response;
    }

    private function isOpenRecord($method){
        if(config('tracerecord.enabled') && !in_array($method,['OPTIONS'])){
            return false;
        }else{
            return true;
        }
    }
}
