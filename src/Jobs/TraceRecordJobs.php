<?php

namespace Dayu\Tracerecord\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class TraceRecordJobs implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request;

    private $response;

    public function __construct($request,$response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function handle(){
        $channel = config('tracerecord.channel');
        //截取返回信息
        $maxResponseSize = config('tracerecord.max_response_size');

        $filepath = config('tracerecord.file_path');

        switch ($channel){
            case 'log':
                Log::channel('tracelog')->info("请求信息：".json_encode($this->request,JSON_UNESCAPED_UNICODE));
                Log::channel('tracelog')->info("返回信息：".($maxResponseSize>0?substr(json_encode($this->response,JSON_UNESCAPED_UNICODE),0,$maxResponseSize):json_encode($this->response,JSON_UNESCAPED_UNICODE)));
                break;
            case 'database':
                break;
            case 'es':
                break;
        }
    }
}
