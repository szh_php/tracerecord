<?php
namespace Dayu\Tracerecord\Providers;

use Illuminate\Contracts\Foundation\CachesConfiguration;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class TraceRecordProvider extends ServiceProvider
{

    /**
     *
     * @return void
     */
    public function boot()
    {
        
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeLoggingConfig();
    }

    //合并logging.php
    public function mergeLoggingConfig(){
        $sourcePath = __DIR__.'/../../config/logging.tmp';
        $configPath = __DIR__.'/../../config/tracerecord.php';
        $outputArray = file_get_contents($sourcePath);
        
        $loggingArray = config('logging');

        if ($this->app->runningInConsole() ){
            if(count($loggingArray) && !isset($loggingArray['channels']['tracelog'])){
                $output = preg_replace('/\'channels.*\=>.*\[/',$outputArray,file_get_contents(config_path('logging.php')),1);
                file_put_contents(config_path('logging.php'),$output);
            }
            $this->publishes([
                $configPath => config_path('tracerecord.php')
            ]);
        }


    }

}
