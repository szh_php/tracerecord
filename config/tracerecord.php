<?php

return [

    'enabled' => (bool) env('TRACERECORD_LOG',false),

    'channel' => env('TRACERECORD_LOG_CHANNEL','log'),
    //记录日志长度 超过这个值 则截断 0表示不截
    'max_response_size' => env('TRACERECORD_MAX_RESPONSE_SIZE',0),
    //日志默认 路径
    'file_path' => env('TRACERECORD_DEFAULT_PATH','logs/api_log/tracerecord.log'),

];